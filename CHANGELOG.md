# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.1

- patch: modifying pipe.yml tags

## 0.2.0

- minor: Added tests and internal refactoring

## 0.1.1

- patch: BITBUCKET_CLONE_DIR is required
- patch: Improved Error messages when a template is not found

## 0.1.0

- minor: using DEBUG instead of ROLLOUT_LOGGER_LEVEL

## 0.0.4

- patch: Using node:10.15.1 instead of node:8.9.0

## 0.0.3

- patch: Adding LICENSE (Apache License, Version 2.0)

## 0.0.2

- patch: internal CI scripts

## 0.0.1

- patch: modified structure to accommodate to semversioner

