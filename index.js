const { 
  ROLLOUT_API_TOKEN,
  ROLLOUT_APP_ID,
  ROLLOUT_ENVIRONMENT = 'Production',
  BITBUCKET_TAG,
  BITBUCKET_BRANCH,
  BITBUCKET_CLONE_DIR,
  ROLLOUT_PIPES_FILENAME = 'rollout-pipes.yaml',
  DEBUG = false
} = process.env;

require('make-promises-safe')
const fs = require('fs');
const jsYaml = require('js-yaml')
const mustache = require('mustache')
const readFile = require('util').promisify(fs.readFile)
const path = require('path');
const axios = require('axios');
const host = 'https://x-api.rollout.io';
const log = (msg) => {
  if (DEBUG === "true") {
    console.log(msg)
  }
}

log(`envs ${JSON.stringify(process.env)}`)



// exported for testing, not scalable but efficient 
module.exports.prepareVars = (vars) => {
  if (!vars.ROLLOUT_FLAG_NAME  || !vars.ROLLOUT_TEMPLATE_NAME) {
    const parsed = module.exports.parseTag(vars.BITBUCKET_TAG || vars.BITBUCKET_BRANCH)
    return Object.assign({}, vars, { ROLLOUT_FLAG_NAME: parsed.flagName, ROLLOUT_TEMPLATE_NAME: parsed.template });
  }
  return vars;
}

// exported for testing, not scalable but efficient 
module.exports.parseTag = (tag) => {
  const match = /(?:.+\/|^\/?)rollout\/([^\/]+)\/([^\/]+)(?:$|\/.+)/.exec(tag);
  if (match != null) {
    return { flagName:match[1], template:match[2] };
  }
  throw `regex not matching ${tag}`;
}

const getParsedTemplate = async (vars) => {
  const pipesPath = path.join(BITBUCKET_CLONE_DIR, ROLLOUT_PIPES_FILENAME);
  const yaml = await readFile(pipesPath, 'utf8')
  log(`yaml ${yaml}`)
  const renderedYaml = mustache.render(yaml, vars);
  log(`rendered yaml ${renderedYaml}`)
  const json = jsYaml.safeLoad(renderedYaml)
  log(`yaml json ${JSON.stringify(json)}`)
  const template = json.templates[vars.ROLLOUT_TEMPLATE_NAME];
  if (!template) {
    throw `could not find template ${vars.ROLLOUT_TEMPLATE_NAME} in ${pipesPath}`;
  }
  return template;
}

const apiCaller = async({ apiTemplate, appKey, environmentName, apiToken }) => {
  const op = {
    method: 'PUT',
    url: `${host}/public-api/${appKey}/${environmentName}/experiments`,
    data: apiTemplate,
    headers: {
      'Authorization': `Bearer ${apiToken}`
    }
  }
  log(`axios options ${JSON.stringify(op)}`)
  return axios(op);
}


if (require.main === module)  {
  if (!ROLLOUT_API_TOKEN || !ROLLOUT_APP_ID || !BITBUCKET_CLONE_DIR) {
    console.error(`missing env variable ROLLOUT_API_TOKEN:${ROLLOUT_API_TOKEN} ROLLOUT_APP_ID:${ROLLOUT_APP_ID} BITBUCKET_CLONE_DIR:${BITBUCKET_CLONE_DIR}`)
    process.exit(-1)
  }

  getParsedTemplate(module.exports.prepareVars(process.env))
    .then((apiTemplate) => {
      log(`apiTemplate ${JSON.stringify(apiTemplate)}`)
      return apiCaller({ apiTemplate, appKey: ROLLOUT_APP_ID, environmentName: ROLLOUT_ENVIRONMENT, apiToken: ROLLOUT_API_TOKEN })
    })
}

