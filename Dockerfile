FROM node:10.15.1

RUN apt-get update && apt-get install -y vim

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn --production

COPY . .

RUN chmod +x start.sh

CMD ["/usr/src/app/start.sh"]
