#!/bin/bash

# Commit back to the repository
# version number, push the tag back to the remote.

set -xe

tag=$(semversioner current-version)
git tag -a -m "Tagging for release ${tag}" "${tag}"
git push origin ${tag}
