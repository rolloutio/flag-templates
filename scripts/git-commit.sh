#!/bin/bash

# Commit back to the repository
# version number, push the tag back to the remote.

set -xe

# Tag and push
tag=$(semversioner current-version)

git add .
git commit -m "Update files for new version '${tag}'" 
git push
