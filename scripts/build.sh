#!/bin/bash 

rev=$(git rev-parse HEAD)
repo=rollout/flag-templates

if [ -z "$1" ] 
then
  docker build -t $repo:$rev . 
  docker push $repo:$rev
else 
  docker build -t $repo:$rev -t $repo:$1 . 
  docker push $repo:$rev
  docker push $repo:$1
fi

