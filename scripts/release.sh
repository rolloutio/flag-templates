#!/bin/bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
$DIR/bump-version.sh
$DIR/build.sh $(semversioner current-version)
$DIR/git-commit.sh
$DIR/git-tag.sh

echo Done






