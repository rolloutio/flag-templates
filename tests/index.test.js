const { parseTag, prepareVars } = require('../index.js')

test('parseTag should throw error if tag does not match required expression', () => {
  expect(() => parseTag("")).toThrow();
  expect(() => parseTag("rollout/")).toThrow();
});

test('parseTag should return a map with value', () => {
  expect(parseTag("rollout/flagA/qa")).toEqual({
    flagName: "flagA",
    template: "qa"
  });
  expect(parseTag("2332321132/rollout/flagA/qa/32132231123")).toEqual({
    flagName: "flagA", 
    template: "qa"
  });
});

test('prepareVars should throw exception if it cant create ROLLOUT_FLAG_NAME or ROLLOUT_TEMPLATE_NAME', () => {
  expect(() => prepareVars({ })).toThrow();
  expect(() => prepareVars({ ROLLOUT_FLAG_NAME: "a"})).toThrow();
  expect(() => prepareVars({ ROLLOUT_TEMPLATE_NAME: "a"})).toThrow();
});

test('prepareVars should return ROLLOUT_FLAG_NAME and ROLLOUT_TEMPLATE_NAME if provided', () => {
  expect(prepareVars({
    ROLLOUT_FLAG_NAME: "a", 
    ROLLOUT_TEMPLATE_NAME: "b"
  })).toEqual({
    ROLLOUT_FLAG_NAME: "a", 
    ROLLOUT_TEMPLATE_NAME: "b"
  });
});
test('prepareVars should return any other var', () => {
  expect(prepareVars({
    anyOther: "var",
    ROLLOUT_FLAG_NAME: "a", 
    ROLLOUT_TEMPLATE_NAME: "b"
  })).toEqual({
    anyOther: "var",
    ROLLOUT_FLAG_NAME: "a", 
    ROLLOUT_TEMPLATE_NAME: "b"
  });
});

test('prepareVars should create ROLLOUT_FLAG_NAME and ROLLOUT_TEMPLATE_NAME from BITBUCKET_TAG when it is provided', () => {
  expect(prepareVars({
    BITBUCKET_TAG: "rollout/a/b"
  })).toEqual({
    ROLLOUT_FLAG_NAME: "a", 
    ROLLOUT_TEMPLATE_NAME: "b", 
    BITBUCKET_TAG:"rollout/a/b"
  });
});

test('prepareVars should create ROLLOUT_FLAG_NAME and ROLLOUT_TEMPLATE_NAME from BITBUCKET_BRANCH when it is provided', () => {
  expect(prepareVars({
    BITBUCKET_BRANCH: "rollout/a/b"
  })).toEqual({
    ROLLOUT_FLAG_NAME: "a", 
    ROLLOUT_TEMPLATE_NAME: "b", 
    BITBUCKET_BRANCH:"rollout/a/b"
  });
});

test('BITBUCKET_TAG is more important then BITBUCKET_BRANCH', () => {
  expect(prepareVars({
    BITBUCKET_TAG: "rollout/a/b",
    BITBUCKET_BRANCH: "rollout/a1/b2"
  })).toEqual({
    ROLLOUT_FLAG_NAME: "a", 
    ROLLOUT_TEMPLATE_NAME: "b", 
    BITBUCKET_BRANCH: "rollout/a1/b2",
    BITBUCKET_TAG:"rollout/a/b"
  });
});
