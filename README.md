# Bitbucket Pipelines Pipe: Rollout.io Flag Template Pipe

Allows you to control your feature flags using templates and git tags with [Rollout.io](https://rollout.io/)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: rolloutio/flag-templates:0.2.1
    variables:
      ROLLOUT_API_TOKEN: '<string>'
      ROLLOUT_APP_ID: '<string>'
      # ROLLOUT_PIPES_FILENAME: '<string>' # Optional.
      # ROLLOUT_ENVIRONMENT: '<string>' # Optional.
      # DEBUG: '<boolean>' # Optional.
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| ROLLOUT_API_TOKEN (*) | Rollout API Token  |
| ROLLOUT_APP_ID (*)    | APP ID for this repo |
| ROLLOUT_PIPES_FILENAME | Pipe configuration file name that defines the templates (Default to `rollout-pipes.yaml`) |
| ROLLOUT_ENVIRONMENT   | Rollout environment (Default `Production`) |
| DEBUG                     | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details

This pipe allows developers configure feature flags using tags or branch names. 

You configure a yaml file that defines different templates for feature flags, when the developer pushes a tag that includes `/rollout/<flag name>/<template>/` in the tag name, this pipe will get executed and do the following:

1. Parse the tag-name / branch-name and extract the name of the flag and the requested template
2. Replace all `{{ SOME_VAR }}` occurrences in `$ROLLOUT_PIPES_FILENAME` with environment variable from type `$SOME_VAR` 
3. Create/Update `flag name`  for app `$ROLLOUT_APP_ID` and environment `$ROLLOUT_ENVIRONMENT` configuration to match the configuration at `$ROLLOUT_PIPES_FILENAME`

## Prerequisites

You should have a a configured Rollout app on Rollout [dashboard](https://app.rollout.io).

## Examples

### Basic example 

The following example configuration allows users to move feature flags using the following templates: 

- internal - opening this feature only for internal employees
- beta - opening this feature for internal employees and beta users
- open - opening this feature for all users


#### bitbucket-pipelines.yml:

```yaml
script:
  - pipe: rolloutio/flag-templates:0.2.1
    variables:
      ROLLOUT_API_TOKEN: '123456'
      ROLLOUT_APP_ID: '123456'
```

#### rollout-pipes.yaml: 
(should be located in repository root dir)

```yaml
templates:
  internal:
    labels: {{ ROLLOUT_TEMPLATE_NAME }}
    flag: {{ ROLLOUT_FLAG_NAME }} 
    type: experiment
    conditions:
      - group:
          name: 
            - Beta Users
            - Internal employees
        value: true
    value: false
  beta:
    labels: {{ ROLLOUT_TEMPLATE_NAME }}
    flag: {{ ROLLOUT_FLAG_NAME }} 
    type: experiment
    conditions:
      - group:
          name: 
            - Beta Users
            - Internal employees
        value: true
    value: false
  open:
    labels: {{ ROLLOUT_TEMPLATE_NAME }}
    flag: {{ ROLLOUT_FLAG_NAME }} 
    type: experiment
    value: true
```
### Using version in template

This example will add a version to open the feature for, this will be done on top of th basic example above.
We will supply the version by running a script in our Repo that allows you to extract the version from the data in the repo, the version itself will be passed to the template by an environment variable TARGETED_VERSION 

#### bitbucket-pipelines.yml:

```yaml
script:
  - pipe: rolloutio/flag-templates:0.2.1
    variables:
      ROLLOUT_API_TOKEN: '123456'
      ROLLOUT_APP_ID: '123456'
      TARGETED_VERSION: $($BITBUCKET_CLONE_DIR/scripts/get_version.sh)
```

#### rollout-pipes.yaml: 
(should be located in repository root dir)

```yaml
templates:
  internal:
    labels: {{ ROLLOUT_TEMPLATE_NAME }}
    flag: {{ ROLLOUT_FLAG_NAME }} 
    type: experiment
    conditions:
      - version: 
          operator: semver-gte
          semver: {{ TARGETED_VERSION }}
        group:
          name: 
            - Beta Users
            - Internal employees
        value: true
    value: false
  beta:
    labels: {{ ROLLOUT_TEMPLATE_NAME }}
    flag: {{ ROLLOUT_FLAG_NAME }} 
    type: experiment
    conditions:
      - version: 
          operator: semver-gte
          semver: {{ TARGETED_VERSION }}
        group:
          name: 
            - Beta Users
            - Internal employees
        value: true
    value: false
  open:
    labels: {{ ROLLOUT_TEMPLATE_NAME }}
    flag: {{ ROLLOUT_FLAG_NAME }} 
    type: experiment
    conditions:
      - version: 
          operator: semver-gte
          semver: {{ TARGETED_VERSION }}
        value: true
    value: false
```


## Support
If you have an issue or feature request, [let us know](https://rollout.io/#popup-premium-contact).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) Rollout, Inc.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
